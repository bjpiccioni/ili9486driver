#ifndef _TANKCONTROLLER_H_
#define _TANKCONTROLLER_H_

#include <avr/eeprom.h>
#include <stdlib.h>

#include "TFT.h"

#define BAUDRATE    115200. //9600.
#define BAUDVAL    ((int) ((float)( F_CPU * 64 / (16 * BAUDRATE))+ 0.5))    //Prescale @ 1 (default is 6)
#define ENT 0x0d
#define ESC 0x1b
#define ABORT   0x03
#define XON     0x11
#define XOFF    0x13

#define enable_interupts() asm("sei")
#define disable_interupts() asm("cli")

#define BITMASK( bit ) ( 1 << bit )
#define TESTBIT( port, bit ) ( port & BITMASK( bit ))

#define FONTSIZE        2

#define DATEANDTIME __TIME__  " " __DATE__
#define LED1ON              PORTE.OUT &= ~0b00000100
#define LED1OFF             PORTE.OUT |= ~0b00000100

#endif
