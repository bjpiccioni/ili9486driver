/*
 * MIT License
 * Copyright (C) 2021 Brian Piccioni brian@documenteddesigns.com
 * @author Brian Piccioni <brian@documenteddesigns.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 */

#include "DriverDemo.h"
#include "TFT.h"
#include "avr/wdt.h"
#include "util/delay.h"

#include <ctype.h>
#include <stdio.h>

FILE    UART_STR;
int     RXBYTES;

void    InitIO( void );
void    InitPorts( void );
int     uart_putchar(char c, FILE *stream);
int     uart_checkchr( );
int     uart_getchar(FILE *stream);
int	    uart_getecho( void );

//
// High level initialize I/O for libraries
//
char    Buffer[60];

int main(void)
{
        disable_interupts();
//
        InitPorts( );                   //Hardware setup
        UART_STR.put = uart_putchar;                // Set up stream to stdio (minimal overhead)
        UART_STR.get = uart_getchar;
        UART_STR.flags = _FDEV_SETUP_RW;
        stdout = stdin = &UART_STR;
        TFTInit( LANDSCAPE );
        printf("\nDisplay ID: %x\n", TFTID );

        wdt_reset();                    //reset WDT interrupt
        wdt_enable( 0x0b ); //Set up a 8 second watch dog (WDT_8S undefined)
        while( true )
        {
            for( uint8_t i = 0; i < 19; i++ )
            {   
                sprintf( Buffer, "#%2d This is a test of the display", (int) i);        
                TFTShowMsgRowCol( i, 0, (char *) Buffer );
            }            
            wdt_reset();                    //reset WDT interrupt
            _delay_ms( 2000 );
            TFTClearDisplay();            
        }
} //main()


//
// Initialize the I/O ports
//
void InitPorts( void )
{
    PORTB.OUT = 0b00111111;             //Control bits high
    PORTB.DIR = 0b00111110;             //Control bits are outputs,POWER_FAIL_INPUT input
    PORTMUX_USARTROUTEA = PORTMUX_USART0_DEFAULT_gc;                            //Usart 0 on PA0, 1
    
    //
    // USART P285
    //
    USART0.BAUDH = BAUDVAL >> 8;
    USART0.BAUDL = BAUDVAL & 0xff;
    USART0.CTRLA = 0;
    USART0.CTRLB = 0b11000000;                  //Enable RX, TX
    USART0.CTRLC = 0b00001011;                  //8 bit 2 stop bits on TX

    CPU_CCP = CCP_IOREG_gc;                     // Enable writing to protected register
    CLKCTRL.MCLKCTRLB = 0;                      //No prescale

    while (RTC.STATUS > 0);                     // Wait for all register to be synchronized
    //
    //  https://raw.githubusercontent.com/MicrochipTech/TB3213_Getting_Started_with_RTC/master/RTC_Periodic_Interrupt/main.c
    //
    CPU_CCP = CCP_IOREG_gc;                     // Enable writing to protected register

    RTC.CLKSEL = RTC_CLKSEL_INT32K_gc;          // 32.768kHz Internal Oscillator (XOSC32K)
    RTC.DBGCTRL = RTC_DBGRUN_bm;                // Run in debug: enabled
    RTC.PITCTRLA = RTC_PERIOD_CYC32_gc          // RTC Clock Cycles /4 = about 1  kHz
    | RTC_PITEN_bm;     // Enabled
    RTC.CLKSEL = RTC_CLKSEL_INT32K_gc;          //
    RTC.PITINTCTRL = RTC_PITEN_bm;              //Enable Interrupt
    //
}


int uart_putchar(char c, FILE *stream)
{
    while( 0 == TESTBIT( USART0_STATUS, USART_DREIF_bp )); //P290
    USART0_TXDATAL = c;
    return( 0 );
}

//
// Returns NZ if a character available
//
int uart_checkchr( )
{
    return( TESTBIT(USART0_STATUS, USART_RXCIF_bp) );
}

int uart_getchar(FILE *stream)
{
    while( uart_checkchr() == 0 );
    return (int) USART0_RXDATAL;
}

int	uart_getecho( void )
{
    int	inp = uart_getchar( NULL );
    uart_putchar( (char) inp, NULL );
    return( inp );
}

