//
// See https://asf.microchip.com/docs/latest/sam3a/html/sam_component_ILI9325_quickstart.html
// and https://asf.microchip.com/docs/latest/sam3a/html/group__ILI9325__display__group.html
//

/**
 * \defgroup ILI9325_display_group Display - ILI9325 Controller
 *
 * Low-level driver for the ILI9325 LCD controller. This driver provides access
 * to the main features of the ILI9325 controller.
 *
 * \{
 */
/*
 * Support and FAQ: visit <a href="https://www.microchip.com/support/">Microchip Support</a>
 */

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "ILI9325.h"
#include "ili9325_regs.h"
#include "TFT.h"


/** LCD X-axis and Y-axis length */
static uint32_t g_ul_lcd_x_length = TFTWidth;
static uint32_t g_ul_lcd_y_length = TFTHeight;

static volatile ILI9325_coord_t limit_start_x, limit_start_y;
static volatile ILI9325_coord_t limit_end_x, limit_end_y;


/**
 * \brief Prepare to write GRAM data for ILI9325.
 */
static void ILI9325_write_ram_prepare(void)
{
		LCD_IR(0);
		LCD_IR(ILI9325_GRAM_DATA_REG);
}

/**
 * \brief Write data to LCD GRAM.
 *
 * \param ul_color 24-bits RGB color.
 */
static void ILI9325_write_ram(ILI9325_color_t ul_color)
{
	LCD_WD((ul_color >> 16) & 0xFF);
	LCD_WD((ul_color >> 8) & 0xFF);
	LCD_WD(ul_color & 0xFF);
}

/**
 * \brief Write multiple data in buffer to LCD controller for ILI9325.
 *
 * \param p_ul_buf data buffer.
 * \param ul_size size in pixels.
 */
static void ILI9325_write_ram_buffer(const ILI9325_color_t *p_ul_buf,
		uint32_t ul_size)
{
	uint32_t ul_addr;
	for (ul_addr = 0; ul_addr < (ul_size - ul_size % 8); ul_addr += 8) {
		ILI9325_write_ram(p_ul_buf[ul_addr]);
		ILI9325_write_ram(p_ul_buf[ul_addr + 1]);
		ILI9325_write_ram(p_ul_buf[ul_addr + 2]);
		ILI9325_write_ram(p_ul_buf[ul_addr + 3]);
		ILI9325_write_ram(p_ul_buf[ul_addr + 4]);
		ILI9325_write_ram(p_ul_buf[ul_addr + 5]);
		ILI9325_write_ram(p_ul_buf[ul_addr + 6]);
		ILI9325_write_ram(p_ul_buf[ul_addr + 7]);
	}
	for (; ul_addr < ul_size; ul_addr++) {
		ILI9325_write_ram(p_ul_buf[ul_addr]);
	}
}

/**
 * \brief Write a word (16bits)to LCD Register.
 *
 * \param uc_reg register address.
 * \param us_data data to be written.
 */
static void ILI9325_write_register_word(uint8_t uc_reg, uint16_t us_data)
{
	LCD_IR(0);
	LCD_IR(uc_reg);
	LCD_WD((us_data >> 8) & 0xFF);
	LCD_WD(us_data & 0xFF);
}

/**
 * \brief Write data to LCD Register for ILI9325.
 *
 * \param uc_reg register address.
 * \param us_data data to be written.
 */
static void ILI9325_write_register(uint8_t uc_reg, uint8_t *p_data,
		uint8_t uc_datacnt)
{
	LCD_IR(0);
	LCD_IR(uc_reg);
	for (uint8_t i = 0; i < uc_datacnt; i++) {
		LCD_WD(p_data[i]);
	}
}

/**
 * \brief Prepare to read GRAM data for ILI9325.
 */
static void ILI9325_read_ram_prepare(void)
{
		LCD_IR(0);
		/** Write Data to GRAM (R22h) */
		LCD_IR(ILI9325_GRAM_DATA_REG);
}

/**
 * \brief Read data to LCD GRAM for ILI9325.
 *
 * \note For ili9325, because pixel data LCD GRAM is 18-bits, so convertion
 * to RGB 24-bits will cause low color bit lose.
 *
 * \return color 24-bits RGB color.
 */
static uint32_t ILI9325_read_ram(void)
{
	uint8_t value[3];
	uint32_t color = 0;
		/** dummy read*/
		value[0] = LCD_RD();
		value[1] = LCD_RD();
		/** data upper byte*/
		value[0] = LCD_RD();
		/** data lower byte */
		value[1] = LCD_RD();

		/** Convert RGB565 to RGB888 */
		/** For BGR format */
		color = ((uint32_t)(value[0] & 0xF8)) | 
                ((uint32_t)(value[0] & 0x07) << 13) |
				((uint32_t)(value[1] & 0xE0) << 5) |
				((uint32_t)(value[1] & 0x1F) << 19);
	return color;
}

/**
 * \brief Read data from LCD Register.
 *
 * \param uc_reg register address.
 * \param p_data the pointer to the read data.
 * \param uc_datacnt the number of the read data
 */
static void ILI9325_read_register(uint8_t uc_reg, uint8_t *p_data,
		uint8_t uc_datacnt)
{
	LCD_IR(0);
	LCD_IR(uc_reg);

	for (uint8_t i = 0; i < uc_datacnt; i++) {
		p_data[i] = LCD_RD();
	}
}

/**
 * \brief Check box coordinates.
 *
 * \param p_ul_x1 X coordinate of upper-left corner on LCD.
 * \param p_ul_y1 Y coordinate of upper-left corner on LCD.
 * \param p_ul_x2 X coordinate of lower-right corner on LCD.
 * \param p_ul_y2 Y coordinate of lower-right corner on LCD.
 */
static void ILI9325_check_box_coordinates(uint32_t *p_ul_x1, uint32_t *p_ul_y1,
		uint32_t *p_ul_x2, uint32_t *p_ul_y2)
{
	uint32_t dw;

	if (*p_ul_x1 >= g_ul_lcd_x_length) {
		*p_ul_x1 = g_ul_lcd_x_length - 1;
	}

	if (*p_ul_x2 >= g_ul_lcd_x_length) {
		*p_ul_x2 = g_ul_lcd_x_length - 1;
	}

	if (*p_ul_y1 >= g_ul_lcd_y_length) {
		*p_ul_y1 = g_ul_lcd_y_length - 1;
	}

	if (*p_ul_y2 >= g_ul_lcd_y_length) {
		*p_ul_y2 = g_ul_lcd_y_length - 1;
	}

	if (*p_ul_x1 > *p_ul_x2) {
		dw = *p_ul_x1;
		*p_ul_x1 = *p_ul_x2;
		*p_ul_x2 = dw;
	}

	if (*p_ul_y1 > *p_ul_y2) {
		dw = *p_ul_y1;
		*p_ul_y1 = *p_ul_y2;
		*p_ul_y2 = dw;
	}
}



void ILI9325_set_window(uint32_t ul_x, uint32_t ul_y, uint32_t ul_width,
		uint32_t ul_height)
{
		/** Set Horizontal Address Start Position */
		ILI9325_write_register_word(ILI9325_HORIZONTAL_ADDR_START,
				(uint16_t)ul_x);

		/** Set Horizontal Address End Position */
		ILI9325_write_register_word(ILI9325_HORIZONTAL_ADDR_END,
				(uint16_t)(ul_x + ul_width - 1));

		/** Set Vertical Address Start Position */
		ILI9325_write_register_word(ILI9325_VERTICAL_ADDR_START,
				(uint16_t)ul_y);

		/** Set Vertical Address End Position */
		ILI9325_write_register_word(ILI9325_VERTICAL_ADDR_END,
				(uint16_t)(ul_y + ul_height - 1));
}


void ILI9325_draw_filled_rectangle(uint32_t ul_x1, uint32_t ul_y1,
		uint32_t ul_x2, uint32_t ul_y2)
{
	uint32_t size, blocks;

	/** Swap coordinates if necessary */
	ILI9325_check_box_coordinates(&ul_x1, &ul_y1, &ul_x2, &ul_y2);

	/** Determine the refresh window area */
	ILI9325_set_window(ul_x1, ul_y1, (ul_x2 - ul_x1) + 1,
			(ul_y2 - ul_y1) + 1);

	/** Set cursor */
	ILI9325_set_cursor_position(ul_x1, ul_y1);

	/** Prepare to write in Graphic RAM */
	ILI9325_write_ram_prepare();

	size = (ul_x2 - ul_x1 + 1) * (ul_y2 - ul_y1 + 1);

	/** Send pixels blocks => one SPI IT / block */
	blocks = size / LCD_DATA_CACHE_SIZE;
	while (blocks--) {
		ILI9325_write_ram_buffer(g_ul_pixel_cache,
								LCD_DATA_CACHE_SIZE);
	}

	/** Send remaining pixels */
	ILI9325_write_ram_buffer(g_ul_pixel_cache,
					size % LCD_DATA_CACHE_SIZE);

	/** Reset the refresh window area */
	ILI9325_set_window(0, 0, g_ul_lcd_x_length, g_ul_lcd_y_length);
}

/**
 * \brief Draw a circle on LCD.
 *
 * \param ul_x X coordinate of circle center.
 * \param ul_y Y coordinate of circle center.
 * \param ul_r circle radius.
 *
 * \return 0 if succeeds, otherwise fails.
 */
uint32_t ILI9325_draw_circle(uint32_t ul_x, uint32_t ul_y, uint32_t ul_r)
{
	int32_t d;
	uint32_t curX;
	uint32_t curY;

	if (ul_r == 0) {
		return 1;
	}

	d = 3 - (ul_r << 1);
	curX = 0;
	curY = ul_r;

	while (curX <= curY) {
		ILI9325_draw_pixel(ul_x + curX, ul_y + curY);
		ILI9325_draw_pixel(ul_x + curX, ul_y - curY);
		ILI9325_draw_pixel(ul_x - curX, ul_y + curY);
		ILI9325_draw_pixel(ul_x - curX, ul_y - curY);
		ILI9325_draw_pixel(ul_x + curY, ul_y + curX);
		ILI9325_draw_pixel(ul_x + curY, ul_y - curX);
		ILI9325_draw_pixel(ul_x - curY, ul_y + curX);
		ILI9325_draw_pixel(ul_x - curY, ul_y - curX);

		if (d < 0) {
			d += (curX << 2) + 6;
		} else {
			d += ((curX - curY) << 2) + 10;
			curY--;
		}

		curX++;
	}

	return 0;
}

/**
 * \brief Draw a filled circle on LCD.
 *
 * \param ul_x X coordinate of circle center.
 * \param ul_y Y coordinate of circle center.
 * \param ul_r circle radius.
 *
 * \return 0 if succeeds, otherwise fails.
 */
uint32_t ILI9325_draw_filled_circle(uint32_t ul_x, uint32_t ul_y, uint32_t ul_r)
{
	signed int d;       /* Decision Variable */
	uint32_t dwCurX;    /* Current X Value */
	uint32_t dwCurY;    /* Current Y Value */
	uint32_t dwXmin, dwYmin;

	if (ul_r == 0) {
		return 1;
	}

	d = 3 - (ul_r << 1);
	dwCurX = 0;
	dwCurY = ul_r;

	while (dwCurX <= dwCurY) {
		dwXmin = (dwCurX > ul_x) ? 0 : ul_x - dwCurX;
		dwYmin = (dwCurY > ul_y) ? 0 : ul_y - dwCurY;
		ILI9325_draw_filled_rectangle(dwXmin, dwYmin, ul_x + dwCurX,
				dwYmin);
		ILI9325_draw_filled_rectangle(dwXmin, ul_y + dwCurY,
				ul_x + dwCurX, ul_y + dwCurY);
		dwXmin = (dwCurY > ul_x) ? 0 : ul_x - dwCurY;
		dwYmin = (dwCurX > ul_y) ? 0 : ul_y - dwCurX;
		ILI9325_draw_filled_rectangle(dwXmin, dwYmin, ul_x + dwCurY,
				dwYmin);
		ILI9325_draw_filled_rectangle(dwXmin, ul_y + dwCurX,
				ul_x + dwCurY, ul_y + dwCurX);

		if (d < 0) {
			d += (dwCurX << 2) + 6;
		} else {
			d += ((dwCurX - dwCurY) << 2) + 10;
			dwCurY--;
		}

		dwCurX++;
	}

	return 0;
}

/**
 * \brief Draw a pixmap on LCD.
 *
 * \param ul_x X coordinate of upper-left corner on LCD.
 * \param ul_y Y coordinate of upper-left corner on LCD.
 * \param ul_width width of the picture.
 * \param ul_height height of the picture.
 * \param p_ul_pixmap pixmap of the image.
 */
void ILI9325_draw_pixmap(uint32_t ul_x, uint32_t ul_y, uint32_t ul_width,
		uint32_t ul_height, const ILI9325_color_t *p_ul_pixmap)
{
	uint32_t size;
	uint32_t dwX1, dwY1, dwX2, dwY2;
	dwX1 = ul_x;
	dwY1 = ul_y;
	dwX2 = ul_x + ul_width;
	dwY2 = ul_y + ul_height;

	/** Swap coordinates if necessary */
	ILI9325_check_box_coordinates(&dwX1, &dwY1, &dwX2, &dwY2);

	/** Determine the refresh window area */
	ILI9325_set_window(dwX1, dwY1, (dwX2 - dwX1 + 1), (dwY2 - dwY1 + 1));

		/** Set cursor */
		ILI9325_set_cursor_position(dwX1, dwY1);
		/** Prepare to write in GRAM */
		ILI9325_write_ram_prepare();

		size = (dwX2 - dwX1) * (dwY2 - dwY1);

		ILI9325_write_ram_buffer(p_ul_pixmap, size);

		/** Reset the refresh window area */
		ILI9325_set_window(0, 0, g_ul_lcd_x_length, g_ul_lcd_y_length);
}
     
/**
 * \internal
 * \brief Helper function to send the drawing limits (boundaries) to the display
 *
 * This function is used to send the currently set upper-left and lower-right
 * drawing limits to the display, as set through the various limit functions.
 *
 * \param send_end_limits  True to also send the lower-right drawing limits
 */
static inline void ILI9325_send_draw_limits(const bool send_end_limits)
{
		/** Set Horizontal Address Start Position */
		ILI9325_write_register_word(ILI9325_HORIZONTAL_ADDR_START,
				(uint16_t)limit_start_x);

		if (send_end_limits) {
			/** Set Horizontal Address End Position */
			ILI9325_write_register_word(ILI9325_HORIZONTAL_ADDR_END,
					(uint16_t)(limit_end_x));
		}

		/** Set Vertical Address Start Position */
		ILI9325_write_register_word(ILI9325_VERTICAL_ADDR_START,
				(uint16_t)limit_start_y);
		if (send_end_limits) {
			/** Set Vertical Address End Position */
			ILI9325_write_register_word(ILI9325_VERTICAL_ADDR_END,
					(uint16_t)(limit_end_y));
		}

		/** GRAM Horizontal/Vertical Address Set (R20h, R21h) */
		ILI9325_write_register_word(ILI9325_HORIZONTAL_GRAM_ADDR_SET,
				limit_start_x);
		ILI9325_write_register_word(ILI9325_VERTICAL_GRAM_ADDR_SET,
				limit_start_y);

}

/**
 * \brief Set the display top left drawing limit
 *
 * Use this function to set the top left limit of the drawing limit box.
 *
 * \param x The x coordinate of the top left corner
 * \param y The y coordinate of the top left corner
 */
void ILI9325_set_top_left_limit(ILI9325_coord_t x, ILI9325_coord_t y)
{
	limit_start_x = x;
	limit_start_y = y;

	ILI9325_send_draw_limits(false);
}

/**
 * \brief Set the display bottom right drawing limit
 *
 * Use this function to set the bottom right corner of the drawing limit box.
 *
 * \param x The x coordinate of the bottom right corner
 * \param y The y coordinate of the bottom right corner
 */
void ILI9325_set_bottom_right_limit(ILI9325_coord_t x, ILI9325_coord_t y)
{
	limit_end_x = x;
	limit_end_y = y;

	ILI9325_send_draw_limits(true);
}

/**
 * \brief Set the full display drawing limits
 *
 * Use this function to set the full drawing limit box.
 *
 * \param start_x The x coordinate of the top left corner
 * \param start_y The y coordinate of the top left corner
 * \param end_x The x coordinate of the bottom right corner
 * \param end_y The y coordinate of the bottom right corner
 */
void ILI9325_set_limits(ILI9325_coord_t start_x, ILI9325_coord_t start_y,
		ILI9325_coord_t end_x, ILI9325_coord_t end_y)
{
	limit_start_x = start_x;
	limit_start_y = start_y;
	limit_end_x = end_x;
	limit_end_y = end_y;

	ILI9325_send_draw_limits(true);
}

/**
 * \brief Read a single color from the graphical memory
 *
 * Use this function to read a color from the graphical memory of the
 * controller.
 *
 * \retval ILI9325_color_t The read color pixel
 */
ILI9325_color_t ILI9325_read_gram(void)
{
	uint8_t value[3];
	ILI9325_color_t color = 0;

		LCD_IR(0);
		/** Write Data to GRAM (R22h) */
		LCD_IR(ILI9325_GRAM_DATA_REG);
		/** two dummy read */
		value[0] = LCD_RD();
		value[1] = LCD_RD();
		/** data upper byte */
		value[0] = LCD_RD();
		/** data lower byte */
		value[1] = LCD_RD();

		/** Convert RGB565 to RGB888 */
		/** For BGR format */
		color = ((uint32_t) (value[0] & 0xF8))       |
				((uint32_t) (value[0] & 0x07) << 13) |
                ((uint32_t) (value[1] & 0xE0) << 5)  |
				((uint32_t) (value[1] & 0x1F) << 19);
	return color;
}

/**
 * \brief Write the graphical memory with a single color pixel
 *
 * Use this function to write a single color pixel to the controller memory.
 *
 * \param color The color pixel to write to the screen
 */
void ILI9325_write_gram(ILI9325_color_t color)
{
    LCD_IR(0);  /** Write Data to GRAM (R22h) */
    LCD_IR(ILI9325_GRAM_DATA_REG);
	LCD_WD((color >> 16) & 0xFF);
	LCD_WD((color >> 8) & 0xFF);
	LCD_WD(color & 0xFF);
}

/**
 * \brief Copy pixels from SRAM to the screen
 *
 * Used to copy a large quantitative of data to the screen in one go.
 *
 * \param pixels Pointer to the pixel data
 * \param count Number of pixels to copy to the screen
 */
void ILI9325_copy_pixels_to_screen(const ILI9325_color_t *pixels,
		uint32_t count)
{
	/** Sanity check to make sure that the pixel count is not zero */
	if( count == 0 ) return;

		LCD_IR(0);
		LCD_IR(ILI9325_GRAM_DATA_REG);

	while (count--) {
		LCD_WD((*pixels >> 16) & 0xFF);
		LCD_WD((*pixels >> 8) & 0xFF);
		LCD_WD(*pixels & 0xFF);
		pixels++;
	}
}

/**
 * \brief Copy pixels from SRAM to the screen
 *
 * Used to copy a large quantitative of data to the screen in one go.
 *
 * \param pixels Pointer to the pixel data
 * \param count Number of pixels to copy to the screen
 */
void ILI9325_copy_raw_pixel_24bits_to_screen(const uint8_t *raw_pixels,
		uint32_t count)
{
	ILI9325_color_t pixels;

	/** Sanity check to make sure that the pixel count is not zero */
	if( count == 0 ) return;

		LCD_IR(0);
		LCD_IR(ILI9325_GRAM_DATA_REG);

	while (count--) {
		pixels = (uint32_t)(*raw_pixels)  |
				(uint32_t)(*(raw_pixels +1)) << 8 |
				(uint32_t)(*(raw_pixels + 2)) << 16;
		LCD_WD((pixels >> 16) & 0xFF);
		LCD_WD((pixels >> 8) & 0xFF);
		LCD_WD(pixels & 0xFF);
		raw_pixels += 3;
	}

}

/**
 * \brief Set a given number of pixels to the same color
 *
 * Use this function to write a certain number of pixels to the same color
 * within a set limit.
 *
 * \param color The color to write to the display
 * \param count The number of pixels to write with this color
 */
void ILI9325_duplicate_pixel(const ILI9325_color_t color, uint32_t count)
{
	/** Sanity check to make sure that the pixel count is not zero */
	if( count == 0 ) return;

		/** Write Data to GRAM (R22h) */
		LCD_IR(0);
		LCD_IR(ILI9325_GRAM_DATA_REG);

	while (count--) {
		LCD_WD((color >> 16) & 0xFF);
		LCD_WD((color >> 8) & 0xFF);
		LCD_WD(color & 0xFF);
	}
}

/**
 * \brief Copy pixels from the screen to a pixel buffer
 *
 * Use this function to copy pixels from the display to an internal SRAM buffer.
 *
 * \param pixels Pointer to the pixel buffer to read to
 * \param count Number of pixels to read
 */
void ILI9325_copy_pixels_from_screen(ILI9325_color_t *pixels, uint32_t count)
{
	/** Remove warnings */
	( void ) pixels;
	( void ) count;
}

