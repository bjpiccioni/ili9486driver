/*
 * MIT License
 * Copyright (C) 2021 Brian Piccioni brian@documenteddesigns.com
 * @author Brian Piccioni <brian@documenteddesigns.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 */

#ifndef TFT_H_
#define TFT_H_

#ifdef __cplusplus
extern "C" {
    #endif

#include <stdlib.h>
#include <stdbool.h>

#include "DisplayPort.h"

//#define     SUPPORT_9325
#define     SUPPORT_9486

//
// See https://asf.microchip.com/docs/latest/sam3x/html/sam_component_ili93xx_quickstart.html
//
#define TFTLCD_DELAY_CMD                0xff

#ifdef SUPPORT_9325
#define ILI9325_DEVICE_CODE_REG         0x00
#define ILI9325_MEMORY_ADDR_CONTROL_REG 0X03
#define ILI9325_DEVICE_CODE             0x9325

#else

#define ILI9486_DEVICE_CODE_REG         0xd3
#define ILI9486_DEVICE_CODE           0x9486

#endif

//
// D15 - D11 is RED, D10 - D5 is GREEN, D4 - D0 is BLUE
//
#define TFT_BLACK       0x0000      /*   0,   0,   0 */
#define TFT_NAVY        0x000F      /*   0,   0, 128 */
#define TFT_DARKGREEN   0x03E0      /*   0, 128,   0 */
#define TFT_DARKCYAN    0x03EF      /*   0, 128, 128 */
#define TFT_MAROON      0x7800      /* 128,   0,   0 */
#define TFT_PURPLE      0x780F      /* 128,   0, 128 */
#define TFT_OLIVE       0x7BE0      /* 128, 128,   0 */
#define TFT_LIGHTGREY   0xC618      /* 192, 192, 192 */
#define TFT_DARKGREY    0x7BEF      /* 128, 128, 128 */
#define TFT_BLUE        0x001F      /*   0,   0, 255 */
#define TFT_GREEN       0x07E0      /*   0, 255,   0 */
#define TFT_CYAN        0x07FF      /*   0, 255, 255 */
#define TFT_RED         0xF800      /* 255,   0,   0 */
#define TFT_MAGENTA     0xF81F      /* 255,   0, 255 */
#define TFT_YELLOW      0xFFE0      /* 255, 255,   0 */
#define TFT_WHITE       0xFFFF      /* 255, 255, 255 */
#define TFT_ORANGE      0xFDA0      /* 255, 180,   0 */
#define TFT_GREENYELLOW 0xB7E0      /* 180, 255,   0 */
#define TFT_PINK        0xFC9F
//
// Display is 320 x 480
// 480 has 40 characters default font, size 2
//
#define FONT_ROWS       7
#define FONT_COLUMNS    5

#define LCDHEIGHT   30
#define LCDWIDTH    40

#define LANDSCAPE   0
#define PORTRAIT    1

#define CHAR_WIDTH		    (( FONT_COLUMNS + 1 ) * 2 )
#define STATUS_CHAR_WIDTH   6
#define LINE_HEIGHT         (( FONT_ROWS + 1 ) * 2 )
#define STATUSROW		    40
#define STATLEN	            10
#define MENULINE            10

#define TIMECOLUMN		0
#define FILESTATCOLUMN	40

#define SWAP16(a, b) {int16_t t = a;a = b;b = t;}

#define TFTClearDisplay()   TFTDrawLine( 0, 0, TFT_WIDTH, TFT_HEIGHT, TFT_BACKGROUND );


struct RegValues16
{
    uint8_t     reg;
    uint16_t    data;
};

extern const    uint8_t DefaultFont[];
extern const    struct RegValues16 ILI9325RegValues[];
extern const    uint8_t ILI9486RegValues[];
extern volatile uint16_t TFTID, TFT_WIDTH, TFT_HEIGHT, TFT_BACKGROUND;

bool    TFTInit( uint8_t aOrientation );
void	TFTSetTextColor( uint16_t aForeground, uint16_t aBackground );
void    TFTShowMsgRowCol(uint8_t aRow, uint8_t aColumn, char *msg );
void    TFTDrawLine(uint16_t aStartX, uint16_t aStartY, uint16_t aLength, uint16_t aHeight, uint16_t aColor);
void    TFTSetBrightness( uint8_t aBrightness );
void    TFTDrawPixel(uint16_t x, uint16_t y, uint16_t color);

#ifdef __cplusplus
}
#endif

#endif /* TFT_H_ */