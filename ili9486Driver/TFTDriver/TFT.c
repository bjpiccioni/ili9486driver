/*
 * MIT License
 * Copyright (C) 2021 Brian Piccioni brian@documenteddesigns.com
 * @author Brian Piccioni <brian@documenteddesigns.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 */

//
// This is my minimal ili9486 driver.
//
// This is meant to replace the customary AdafruitGFX + MCUFriend_kbv package. Unfortunately
// those suffer from the customary Arduino code bloat and typically use so much program
// memory there is little left for an application. Ever after serious editing the Arduino
// drivers take up a huge amount of space and are very slow.
//
// Roughly speaking this driver requires about 1200 bytes to print a message on the display
// including about 400 bytes for the font. It is also fast so you can fill the display in
// about 0.5 seconds with a 20Mhz Atmega4809.
//
// It is meant to driver a typical Arduino 3.5" display with an ili9486 controller. I also
// have ili9325 support but I haven't tested it yet as I can't remember where I put my 9325
// diplay. Note that most display controllers are quite similar so adapting to a new
// controller should be straightforeward.
//
// There are 6 externally callable routines
//
// 1) Initialize display aOrientation = 0, landscape
//      bool TFTInit( uint8_t aOrientation );    
//
// 2) Set foreground/background colour
//      void	TFTSetTextColor( uint16_t aForeground, uint16_t aBackground );
//
// 3) Display a text message @ row, column. Character set 20 to 0x7f
//    Note that fonts tend to be on their side to save memory but this makes them slow to
//    draw a pixel at a time. The controllers tend to have a "fast write" function which 
//    allows you to blast pixels to the display very quickly. In order to exploit this 
//    feature without remapping the font I switch the display on its side, draw the text
//    then switch the display back. Keep that in mind if you want to port to a different
//    controller.
//      void TFTShowMsgRowCol(uint8_t aRow, uint8_t aColumn, char *msg );
//
// 4) Draw a straight horizontal or vertical line of length, width of colour. This is also 
//    used to clear the display
//      void TFTDrawLine(uint16_t aStartX, uint16_t aStartY, uint16_t aLength, uint16_t aHeight, uint16_t aColor);
//
// 5) Draw a Pixel @x, y of colour. This can be used to do pretty much anything but is as slow
//    as the Arduino driver (probably)
//      void TFTDrawPixel(uint16_t x, uint16_t y, uint16_t color);
//
// 6) Set the display brightness LED (if supported)
//      void TFTSetBrightness( uint8_t aBrightness );
//


#include <stdbool.h>
#include <avr/eeprom.h>
#include <util/delay.h>

#include "TFT.h"
#if (defined( SUPPORT_9325 ) && defined ( SUPPORT_9486 )) \
      || ( !defined( SUPPORT_9325 ) && !defined ( SUPPORT_9486 ))
        #error "Have to choose only 9325 or 9486"
#endif

#ifdef SUPPORT_9325
    #pragma message "Support 9325 TFT Display"
#endif

#ifdef SUPPORT_9486
    #pragma message "Support 9486 TFT Display"
#endif

volatile    uint16_t    TFT_FOREGROUND = TFT_WHITE, TFT_BACKGROUND = TFT_BLACK;
volatile    uint16_t    TFTID, TFT_HEIGHT = 320, TFT_WIDTH = 480;

#ifdef  SUPPORT_9486
    #define LCD_MEM_WRITE_REG 0x2C      //Memory Write
#else
    #define LCD_MEM_WRITE_REG 0x22      //Write Data to GRAM (16 bits) write data inc/dec address
#endif

uint16_t    LCD_MEMORY_ADDRCONTROL_GRAPHIC, LCD_MEMORY_ADDRCONTROL_FONT;

void        TFTDrawChar(uint16_t aStartX, uint16_t aStartY, unsigned char c, uint8_t aSize);


static void TFTWriteArray(uint8_t reg, int8_t arraylen, uint8_t * array)
{
    TFT_SetRegister(reg);
    for( uint8_t i = 0; i < arraylen; i++ )
    TFT_Write8( *array++ );
    TFT_CSIdle();
}

static void    TFTSetMemoryAddressControl( uint16_t aMACVal )
{
#ifdef SUPPORT_9486    
        TFTWriteArray( 0x36, 1, (uint8_t *) &aMACVal);
#else
#pragma message "Fix"
        TFTWriteRegister(ILI9325_MEMORY_ADDR_CONTROL_REG, aMACVal);    // set GRAM write direction and BGR=1.
#endif
}


//
// Clip window coordinates to fit inside the display and make sure aEndX > aStartX and aEndY > aStartY
//
static void TFTNormalizeWindow(uint16_t *aStartX, uint16_t *aStartY, uint16_t *aEndX, uint16_t *aEndY)
{
    if( *aStartX > *aEndX )    SWAP16( *aStartX, *aEndX );
    if( *aStartY > *aEndY )    SWAP16( *aStartY, *aEndY );
}    

//
// Defining a window determines the "box" memory addresses are written in to see P181 of 9486 Data sheet
// So to paint a character you define the "box as col, row, col + #line, row + #cols then write out all
// of the bits. A 5x7 character has 5 colums and 7 rows  
//
static void TFTSetAddrWindow(uint16_t aStartX, uint16_t aStartY, uint16_t aEndX, uint16_t aEndY)
{
    TFTNormalizeWindow( &aStartX, &aStartY, &aEndX, &aEndY );

#ifdef SUPPORT_9486
uint8_t commandsx[4] = { aStartX >> 8, aStartX, aEndX >> 8, aEndX };     //Set LCD_MC, LCD_SC, LCD_EC
uint8_t commandsy[4] = { aStartY >> 8, aStartY, aEndY >> 8, aEndY };

    TFTWriteArray(0x2A, 4, commandsx );          //Set start and end of X
    TFTWriteArray(0x2B, 4, commandsy );          //Set the start and end of Y 
#else

    TFTWriteRegister(0x20, aStartX);    //Horizontal GRAM Set (AD7 - AD0 bits in HL) P67
    TFTWriteRegister(0x21, aStartY);    //Verttical GRAM Address Set (AD16 - AD8 bits in HL)

    if((aStartX == aEndX ) && ( aStartY == aEndY)) return;  //Just a pixel
    TFTWriteRegister(0x50, aStartX);    //Horizontal Window Start Position (HSA7 - HSA0)
    TFTWriteRegister(0x52, aStartY);    //Vertical Address Start Position (VSA8 - VSA0)
    TFTWriteRegister(0x51, aEndX);      //Horizontal End Position (HEA7 - HEA0)
    TFTWriteRegister(0x53, aEndY);      //Vertical Address End Position (VEA8 - VEA0)
#endif
}

static void TFTDelay( uint16_t aMsecDelay )
{
    for( uint16_t i = 0; i < aMsecDelay; i++)
        _delay_ms(1);
}

#ifdef  SUPPORT_9325

static void    TFTInit9325( void )
{
uint8_t     reg;        //Registers are all 8 bit values
uint16_t    data;       //Data is 16 bits
struct      RegValues16 *InitTable = ( struct RegValues16 *) &ILI9325RegValues;

    LCD_MEMORY_ADDRCONTROL_GRAPHIC = 0x1038;    //Assume landscape
    LCD_MEMORY_ADDRCONTROL_FONT    = 0x1030;    //Font is rotated
    do
    {
        reg = InitTable->reg;
        data = InitTable->data;
        if (reg == TFTLCD_DELAY_CMD)
            if( data == 0 ) return; else TFTDelay( data );
        else
            TFTWriteRegister(reg, data);
        
        ++InitTable;
    }while( true );
}

#else
//
// table is reg, size, list of values
// end is is delay(0)
//
// If Memory Access Control (36h) B5 = 0: MX
// The column and page registers are reset to the Start Column (SC) and Start Page (SP)
// Pixel Data 1 is stored in frame memory at (SC, SP). The column register is then incremented
// and pixels are written to the frame memory until the column register equals the End Column
// (EC) value. The column register is then reset to SC and the page register is incremented.
// Bit 2 MH  04
// Bit 3 BGR 08   set up RGB else BGR
// Bit 4 ML  10   vertical scrolling (p187)
// Bit 5 MV  20   fill direction (p181)
//                if 0, column++ to end, then ++row
//                if 1, row++ to end then ++column
// Bit 6 MX  40   row address order top to bottom when 0 (p156)
// Bit 7 MY  80   column address order left to right when 0 ( p156)
//
static void    TFTInit9486( void )
{
uint8_t     reg;        //Registers are all 8 bit values
uint8_t     size;       //Data is 16 bits
uint8_t     *table = ( uint8_t*) &ILI9486RegValues;

    LCD_MEMORY_ADDRCONTROL_GRAPHIC = 0x28;   //Assume landscape MV RGB 
    LCD_MEMORY_ADDRCONTROL_FONT    = 0x08;   //Font is rotated MX RGB

    do
    {
        reg = table[0];
        size = table[1];
        table += 2;         //Skip these
        if (reg == TFTLCD_DELAY_CMD)
            if( size == 0 ) return; else TFTDelay( size );
        else
        {
            TFTWriteArray(reg, size, table );
            table += size;
        }
    }while( true );
}
#endif
    
// ***********************************************************************************//
//  These routines are visible to outside 
// ***********************************************************************************//
//
// Initalize the display
// Returns false if unsupported type
// Set orientation == 0 landscape
//  

bool    TFTInit( uint8_t aOrientation )
{
//Perform a hardware reset    
    TFT_ResetActive();
    TFTDelay(50);
    TFT_ResetIdle();
//    
    TFTDelay(50);
#ifdef SUPPORT_9486

    TFT_SetRegister( ILI9486_DEVICE_CODE_REG );
    TFTID = TFT_Read16bits();    //Ignore dummy read and version number
    TFTID = TFT_Read16bits();    //Second word is Device ID
    TFT_CSIdle();
    if(TFTID  != ILI9486_DEVICE_CODE ) return false;
    TFTInit9486();
#else

    TFT_SetRegister( ILI9325_DEVICE_CODE_REG );
    TFTID = TFT_Read16bits();
    TFT_CSIdle();
    if ( TFTID != ILI9325_DEVICE_CODE) return false;
    TFTInit9325( );
#pragma message "Check 9325 settings especially these two"

    TFTWriteRegister(0x60, 0xa700);    // Gate Scan Line (0xA700) Driver Output Control
                                       //NL 0x3f, SCN = 0
    TFTWriteRegister(0x01, aOrientation == 0 ? ( 1 << 8 ) : 0 );     // set Driver Output Control
                                                //Driver Output Control 1 SS set/clear (p53)
#endif

    TFT_HEIGHT = 320;
    TFT_WIDTH  = 480;
        
    if( aOrientation != 0 ) //Portrait mode
    {
        SWAP16( LCD_MEMORY_ADDRCONTROL_GRAPHIC, LCD_MEMORY_ADDRCONTROL_FONT );
        SWAP16( TFT_HEIGHT, TFT_WIDTH );
    }
                
    TFTSetTextColor( TFT_WHITE, TFT_BLACK );
    TFTClearDisplay();          
    TFTSetBrightness( 100 );
    return true;
}

//
// for size see ADAFruit_GFX.c line 1136 - 1237
//
void TFTShowMsgRowCol(uint8_t aRow, uint8_t aColumn, char *aMessage )
{
uint8_t     size = ( aRow == STATUSROW ? 1 : 2 );   //Set small size for status row
uint16_t    x = aColumn * ( size * ( FONT_COLUMNS + 1 ));
uint16_t    y = aRow * ( size * ( FONT_ROWS  + 1 ));
            if( aRow == STATUSROW )
                y = TFT_HEIGHT - FONT_ROWS - 1;

    TFTSetMemoryAddressControl( LCD_MEMORY_ADDRCONTROL_FONT );  //Font orienntation is sideways so save space
	while (*aMessage != 0) 
    { 
        TFTDrawChar( y, x, *aMessage++, size );     //Note that this also changes display orientation
		x += (( FONT_COLUMNS + 1) * size );
    }
    TFTSetMemoryAddressControl( LCD_MEMORY_ADDRCONTROL_GRAPHIC );  //Set it back
}

//
// Draw a character starting at XY, clear background for character first
// Built in is ASCII 5x7 font zoom accoring to size
// The font is sidesways (rotated 90 degrees) to save space
// Therefore, the area where the text will be written is rotated by calling
// TFTSetMemoryAddressControl( LCD_MEMORY_ADDRCONTROL_FONT ) before calling TFTDrawChar
// This essentialy temporarily rotates the display so now X and Y are swapped
// The font can be scaled (x2) to make larger characters. Beyond x2 it looks really blocky 
//
volatile uint8_t myfont;

void TFTDrawChar(uint16_t aStartX, uint16_t aStartY, unsigned char c, uint8_t aSize) 
{
bool     scale, first;
uint8_t  fontcolumn, *fontpointer;
uint16_t EndX, EndY, thispixel;
 
    if(( aSize < 1 ) || ( aSize > 2 ))
             aSize = 1;

    EndX = aStartX + ( aSize * ( 1 + FONT_COLUMNS ));
    EndY = aStartY + ( aSize * ( 1 + FONT_ROWS ));

    c -= ' ';
    if(( c < 0 ) || ( aStartY > TFT_WIDTH ) || ( aStartX > TFT_HEIGHT )) //Remember X, Y swapped
        return;   //No unprintables no off screen

    scale = ( aSize == 2 );
    TFTSetAddrWindow(aStartX, aStartY, EndX + aSize + 1, EndY + aSize + 1  );         //Define the window

    fontpointer = (uint8_t*) &DefaultFont[c * FONT_COLUMNS];
int8_t columncount = FONT_COLUMNS;
    first = true;
    
    TFT_SetRegister(LCD_MEM_WRITE_REG);         //Set to write. Writing starts at the window 
    do //Write all the pixels out nonstop. The controller worry about where they go
    {
        if( columncount > 0 )
            fontcolumn = *fontpointer;
        else
            fontcolumn = 0;

        myfont = fontcolumn;
        
        for( uint8_t j = 0; j < ( FONT_ROWS + 1 ); ++j )
        {
            thispixel = ( fontcolumn & 1 ) ? TFT_FOREGROUND : TFT_BACKGROUND;
            TFT_Write16( thispixel );
            if( scale ) TFT_Write16( thispixel );    //Zoomed so duplicate the pixel
            fontcolumn >>= 1;
        }            
        if( scale && first ) //If scaling, duplicate the column
            first = false;
        else
        {   
            first = true;   //and maybe duplicate next time
            ++fontpointer;  //else move to next column
            --columncount;
        }        
    }while( columncount >= 0 );    

    TFT_CSIdle();    //Done with writing
}

void	TFTSetTextColor( uint16_t aForeground, uint16_t aBackground )
{
    TFT_FOREGROUND = aForeground;
    TFT_BACKGROUND = aBackground;
}

void    TFTSetBrightness( uint8_t aBrightness )
{   //It seems only 9486 has a brightness control
    #ifdef SUPPORT_9486
    TFTWriteArray( 0x51, 1, (uint8_t *) &aBrightness);
    #endif
}


//
// This draws a single pixel
// Note that it is faster to use draw line if more than one adjacent pixel same colour
//
void TFTDrawPixel(uint16_t x, uint16_t y, uint16_t color)
{
    if (( x >= TFT_WIDTH ) || ( y >= TFT_HEIGHT )) return; //Off screen

    TFTSetAddrWindow(x, y, x, y);
    TFT_WriteRegister(LCD_MEM_WRITE_REG, color);
}

//
// Draw line does a fast filled rectangle drawing
// This means it can fill the display, (width, length is display dimensions)
// clear the display or part thereof, (aColor is background)
// draw a single pixel horizontal or vertical line, etc
//
void TFTDrawLine(uint16_t aStartX, uint16_t aStartY, uint16_t aLength, uint16_t aHeight, uint16_t aColor)
{
    if(( aLength == 0 ) || (aHeight == 0 )) return;
    uint16_t EndX = aStartX + aLength - 1;
    uint16_t EndY = aStartY + aHeight - 1;
    uint32_t pixels;

    
    TFTSetMemoryAddressControl( LCD_MEMORY_ADDRCONTROL_GRAPHIC );
    TFTNormalizeWindow( &aStartX, &aStartY, &EndX, &EndY ); //Normalize here to get proper dimensions
    TFTSetAddrWindow(aStartX, aStartY, EndX, EndY);
    
    pixels = (uint32_t) (EndX - aStartX + 1 ) * (uint32_t) ( EndY - aStartY + 1);    //After normalization
    TFT_SetRegister(LCD_MEM_WRITE_REG);
    for( uint32_t i = 0; i < pixels; i++ )
    TFT_Write16( aColor );

    TFT_SetRegister(0);      //Noop
    TFT_CSIdle();
}

