/**
 * \file
 *
 * \brief API driver for ILI9325 TFT display component.
 *
 * Copyright (c) 2013-2018 Microchip Technology Inc. and its subsidiaries.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Subject to your compliance with these terms, you may use Microchip
 * software and any derivatives exclusively with Microchip products.
 * It is your responsibility to comply with third party license terms applicable
 * to your use of third party software (including open source software) that
 * may accompany Microchip software.
 *
 * THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES,
 * WHETHER EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE,
 * INCLUDING ANY IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY,
 * AND FITNESS FOR A PARTICULAR PURPOSE. IN NO EVENT WILL MICROCHIP BE
 * LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, INCIDENTAL OR CONSEQUENTIAL
 * LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND WHATSOEVER RELATED TO THE
 * SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS BEEN ADVISED OF THE
 * POSSIBILITY OR THE DAMAGES ARE FORESEEABLE.  TO THE FULLEST EXTENT
 * ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN ANY WAY
 * RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY,
 * THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.
 *
 * \asf_license_stop
 *
 */
/*
 * Support and FAQ: visit <a href="https://www.microchip.com/support/">Microchip Support</a>
 */

#ifndef ILI9325_H_INCLUDED
#define ILI9325_H_INCLUDED

/**
 * \defgroup ILI9325_display_group - LCD with ILI9325 component driver
 *
 * This is a driver for LCD with ILI9325. 
 * The driver provides functions for initializtion and control of the LCD.
 *
 * See \ref sam_component_ILI9325_quickstart.
 *
 * \{
 */


/** @cond 0 */
/**INDENT-OFF**/
#ifdef __cplusplus
extern "C" {
#endif
/**INDENT-ON**/
/** @endcond */

#include "DisplayPort.h"

/** Type define for an integer type large enough to store a pixel color. */
typedef uint32_t ILI9325_color_t;

/** Type define for an integer type large enough to store a pixel coordinate.
 */
typedef int16_t ILI9325_coord_t;

/** This macro generates a 16-bit native color for the display from a
 *  24-bit RGB value.
 */
#define ILI9325_COLOR(r, g, b) ((r << 16) | (g << 8) | b)

typedef ILI9325_color_t gfx_color_t;
typedef int16_t gfx_coord_t;

/** Bit mask for flipping X for ILI9325_set_orientation() */
#define ILI9325_FLIP_X 1
/** Bit mask for flipping Y for ILI9325_set_orientation() */
#define ILI9325_FLIP_Y 2
/** Bit mask for swapping X and Y for ILI9325_set_orientation() */
#define ILI9325_SWITCH_XY 4

/** ILI9325 ID code */
#define ILI9486_DEVICE_CODE 0x9486
#define ILI9325_DEVICE_CODE 0x9325

/** Define EBI access for ILI9325 8-bit System Interface.*/

/** RGB 24-bits color table definition (RGB888). */
#define COLOR_BLACK          (0x000000u)
#define COLOR_WHITE          (0xFFFFFFu)
#define COLOR_BLUE           (0x0000FFu)
#define COLOR_GREEN          (0x00FF00u)
#define COLOR_RED            (0xFF0000u)
#define COLOR_NAVY           (0x000080u)
#define COLOR_DARKBLUE       (0x00008Bu)
#define COLOR_DARKGREEN      (0x006400u)
#define COLOR_DARKCYAN       (0x008B8Bu)
#define COLOR_CYAN           (0x00FFFFu)
#define COLOR_TURQUOISE      (0x40E0D0u)
#define COLOR_INDIGO         (0x4B0082u)
#define COLOR_DARKRED        (0x800000u)
#define COLOR_OLIVE          (0x808000u)
#define COLOR_GRAY           (0x808080u)
#define COLOR_SKYBLUE        (0x87CEEBu)
#define COLOR_BLUEVIOLET     (0x8A2BE2u)
#define COLOR_LIGHTGREEN     (0x90EE90u)
#define COLOR_DARKVIOLET     (0x9400D3u)
#define COLOR_YELLOWGREEN    (0x9ACD32u)
#define COLOR_BROWN          (0xA52A2Au)
#define COLOR_DARKGRAY       (0xA9A9A9u)
#define COLOR_SIENNA         (0xA0522Du)
#define COLOR_LIGHTBLUE      (0xADD8E6u)
#define COLOR_GREENYELLOW    (0xADFF2Fu)
#define COLOR_SILVER         (0xC0C0C0u)
#define COLOR_LIGHTGREY      (0xD3D3D3u)
#define COLOR_LIGHTCYAN      (0xE0FFFFu)
#define COLOR_VIOLET         (0xEE82EEu)
#define COLOR_AZUR           (0xF0FFFFu)
#define COLOR_BEIGE          (0xF5F5DCu)
#define COLOR_MAGENTA        (0xFF00FFu)
#define COLOR_TOMATO         (0xFF6347u)
#define COLOR_GOLD           (0xFFD700u)
#define COLOR_ORANGE         (0xFFA500u)
#define COLOR_SNOW           (0xFFFAFAu)
#define COLOR_YELLOW         (0xFFFF00u)

/**
 * Input parameters when initializing ili9325 driver.
 */
struct ILI9325_opt_t {
	/** LCD width in pixel*/
	uint32_t ul_width;
	/** LCD height in pixel*/
	uint32_t ul_height;
	/** LCD foreground color*/
	uint32_t foreground_color;
	/** LCD background color*/
	uint32_t background_color;
};

/**
 * Font structure
 */
struct ILI9325_font {
	/** Font width in pixels. */
	uint8_t width;
	/** Font height in pixels. */
	uint8_t height;
};

/**
 * Display direction option
 */
enum ILI9325_display_direction {
	LANDSCAPE  = 0,
	PORTRAIT   = 1
};

/**
 * Shift direction option
 */
enum ILI9325_shift_direction {
	H_INCREASE  = 0,
	H_DECREASE  = 1
};

/**
 * Scan direction option
 */
enum ILI9325_scan_direction {
	V_INCREASE  = 0,
	V_DEREASE   = 1
};

uint32_t ILI9325_init(struct ILI9325_opt_t *p_opt);
void    ILI9325_display_on(void);
void    ILI9325_display_off(void);
void    ILI9325_set_foreground_color(ILI9325_color_t ul_color);
void    ILI9325_fill(ILI9325_color_t ul_color);
void    ILI9325_set_window(uint32_t ul_x, uint32_t ul_y,
		    uint32_t ul_width, uint32_t ul_height);
void    ILI9325_set_cursor_position(uint16_t us_x, uint16_t us_y);
void    ILI9325_scroll(int32_t ul_lines);
void    ILI9325_enable_scroll(void);
void    ILI9325_disable_scroll(void);
void    ILI9325_set_display_direction(enum ILI9325_display_direction e_dd,
		    enum ILI9325_shift_direction e_shd,
		    enum ILI9325_scan_direction e_scd);
uint32_t ILI9325_draw_pixel(uint32_t ul_x, uint32_t ul_y);
ILI9325_color_t ILI9325_get_pixel(uint32_t ul_x, uint32_t ul_y);
void    ILI9325_draw_line(uint32_t ul_x1, uint32_t ul_y1,
		    uint32_t ul_x2, uint32_t ul_y2);
void    ILI9325_draw_rectangle(uint32_t ul_x1, uint32_t ul_y1,
		    uint32_t ul_x2, uint32_t ul_y2);
void    ILI9325_draw_filled_rectangle(uint32_t ul_x1, uint32_t ul_y1,
		    uint32_t ul_x2, uint32_t ul_y2);
uint32_t ILI9325_draw_circle(uint32_t ul_x, uint32_t ul_y, uint32_t ul_r);
uint32_t ILI9325_draw_filled_circle(uint32_t ul_x, uint32_t ul_y,
		    uint32_t ul_r);
void    ILI9325_draw_string(uint32_t ul_x, uint32_t ul_y, const uint8_t *p_str);
void    ILI9325_draw_pixmap(uint32_t ul_x, uint32_t ul_y, uint32_t ul_width,
		    uint32_t ul_height, const ILI9325_color_t *p_ul_pixmap);
void    ILI9325_set_top_left_limit(ILI9325_coord_t x, ILI9325_coord_t y);
void    ILI9325_set_bottom_right_limit(ILI9325_coord_t x, ILI9325_coord_t y);
void    ILI9325_set_limits(ILI9325_coord_t start_x, ILI9325_coord_t start_y,
		    ILI9325_coord_t end_x, ILI9325_coord_t end_y);
ILI9325_color_t ILI9325_read_gram(void);
void    ILI9325_write_gram(ILI9325_color_t color);
void    ILI9325_copy_pixels_to_screen(const ILI9325_color_t *pixels,
		    uint32_t count);
void    ILI9325_copy_raw_pixel_24bits_to_screen(const uint8_t *raw_pixels,
		    uint32_t count);
void    ILI9325_duplicate_pixel(const ILI9325_color_t color, uint32_t count);
void    ILI9325_copy_pixels_from_screen(ILI9325_color_t *pixels, uint32_t count);
uint8_t ILI9325_device_type(void);
void    ILI9325_vscroll_area_define(uint16_t us_tfa, uint16_t us_vsa,
		    uint16_t us_bfa);
uint16_t ILI9325_device_type_identify(void);
void    ILI9325_set_orientation(uint8_t flags);
uint32_t ILI9325_get_lcd_type(void);


/** @cond 0 */
/**INDENT-OFF**/
#ifdef __cplusplus
}
#endif
/**INDENT-ON**/
/** @endcond */

#endif /* ILI9325_H_INCLUDED */
