//
// These are board specific routines
// Note that the timing of TFT_ read and write can vary a lot depending on CPU
// and clock frequency so you might have to add delays. Read is especially slow
//
// I use static (not outside visible) inline functions for speed. A faster processor such as an SAM51
// they could probably just be normal functions
//
// These particular functions are set up for a proprietary Atmega4809. They need to be modified for 
// use in other boards. 
//
#ifndef DISPLAYPORT_H
#define DISPLAYPORT_H

#ifdef __cplusplus
extern "C" {
    #endif

#include <avr\io.h>

#define RD_PIN      1
#define WR_PIN      2
#define CD_PIN      3
#define CS_PIN      4
#define RESET_PIN   5

#define DisplayOut  PORTC_OUT
#define DisplayIn   PORTC_IN

static inline void TFT_CSActive( void )     { PORTB_OUTCLR = ( 1 << CS_PIN );}
static inline void TFT_CSIdle( void )       { PORTB_OUTSET = ( 1 << CS_PIN );}
static inline void TFT_ResetActive( void )  { PORTB_OUTCLR = ( 1 << RESET_PIN );}
static inline void TFT_ResetIdle( void )    { PORTB_OUTSET = ( 1 << RESET_PIN );}
static inline void TFT_RDActive( void )     { PORTB_OUTCLR = ( 1 << RD_PIN );}
static inline void TFT_RDIdle( void )       { PORTB_OUTSET = ( 1 << RD_PIN );}
static inline void TFT_WRActive( void )     { PORTB_OUTCLR = ( 1 << WR_PIN );}
static inline void TFT_WRIdle( void )       { PORTB_OUTSET = ( 1 << WR_PIN );}
static inline void TFT_RSCommand( void )    { PORTB_OUTCLR = ( 1 << CD_PIN );}
static inline void TFT_RSData( void )       { PORTB_OUTSET = ( 1 << CD_PIN );}

static inline void TFT_SetReadDir( void )    { PORTC_DIR = 0;}
static inline void TFT_SetWriteDir( void )   { PORTC_DIR = 0xff;}

static inline void TFT_SetRead( void )
{
    TFT_SetReadDir();
    TFT_RDActive();
}

static inline void TFT_SetWrite( void )
{
    TFT_SetWriteDir();
    TFT_CSActive();
}


static inline void TFT_Write8( uint8_t val )
{
    DisplayOut = val;
    TFT_WRActive();
    TFT_WRIdle();
}

static inline void TFT_Write16( uint16_t val )
{
    TFT_Write8( ( uint8_t ) ( val >> 8 ));
    TFT_Write8( ( uint8_t ) val );
}

static inline void TFT_SetRegister( uint8_t reg )
{
    TFT_RSCommand();
    TFT_SetWrite();
    TFT_Write16( reg );
    TFT_RSData();
}

static inline void TFT_WriteRegister(uint8_t cmd, uint16_t dat)
{
    TFT_SetRegister( cmd );
    TFT_Write16( dat );
    TFT_CSIdle();
}

static inline uint16_t TFT_Read16bits(void)
{
uint16_t    ret;
uint8_t     lo;

    TFT_SetRead();
    TFT_SetRead();       //Basically a delay
    ret = DisplayIn;
    TFT_RDIdle();
    TFT_RDIdle();
    TFT_SetRead();
    TFT_SetRead();
    lo = DisplayIn;
    TFT_RDIdle();
    return (ret << 8) | lo;
}




#ifdef __cplusplus
}
#endif

#endif