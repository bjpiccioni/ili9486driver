/*
 * Fuses.c
 * Note this has to be a c file or it will not compile
 */ 

#include <avr/io.h>
FUSES = {
	.APPEND = 0x00,
	.BODCFG = 0x00,
	.BOOTEND = 0x00,			
	.OSCCFG = FREQSEL_20MHZ_gc,		//FREQSEL_20MHZ_gc
	.SYSCFG0 = RSTPINCFG_RST_gc		//Reset input enabled
				+ CRCSRC_NOCRC_gc	//No CRC
				+ FUSE_EESAVE_bm,	//Leave eeprome alone		
	.SYSCFG1 = SUT_64MS_gc,			//StartUp delay 64 mSec
	.WDTCFG = 0x00,					//Disable Watchdog Timer .WDTCFG = PERIOD_OFF_gc,
};


