This is my minimal ili9486 driver.

This is meant to replace the customary AdafruitGFX + MCUFriend_kbv package. Unfortunately those suffer from the customary Arduino code bloat and typically use so much program memory there is little left for an application. Ever after serious editing the Arduino drivers take up a huge amount of space and are very slow.

Roughly speaking this driver requires about 1200 bytes to print a message on the display including about 400 bytes for the font. It is also fast so you can fill the display in about 0.5 seconds with a 20Mhz Atmega4809.

It is meant to drive a typical Arduino 3.5" display with an ili9486 controller. I also have ili9325 support but I haven't tested it yet as I can't remember where I put my 9325 diplay. Note that most display controllers are quite similar so adapting to a new controller should be straightforeward.

There are 6 externally callable routines

1) Initialize display aOrientation = 0, landscape
     bool TFTInit( uint8_t aOrientation );    

2) Set foreground/background colour
     void	TFTSetTextColor( uint16_t aForeground, uint16_t aBackground );

3) Display a text message @ row, column. Character set 20 to 0x7f
   Note that fonts tend to be on their side to save memory but this makes them slow to
   draw a pixel at a time. The controllers tend to have a "fast write" function which 
   allows you to blast pixels to the display very quickly. In order to exploit this 
   feature without remapping the font I switch the display on its side, draw the text
   then switch the display back. Keep that in mind if you want to port to a different
   controller.
     void TFTShowMsgRowCol(uint8_t aRow, uint8_t aColumn, char *msg );

4) Draw a straight horizontal or vertical line of length, width of colour. This is also 
   used to clear the display
     void TFTDrawLine(uint16_t aStartX, uint16_t aStartY, uint16_t aLength, uint16_t aHeight, uint16_t aColor);

5) Draw a Pixel @x, y of colour. This can be used to do pretty much anything but is as slow
   as the Arduino driver (probably)
     void TFTDrawPixel(uint16_t x, uint16_t y, uint16_t color);

6) Set the display brightness LED (if supported)
     void TFTSetBrightness( uint8_t aBrightness );


Any quesitons or bugs contact me brian(at)documenteddesigns.com
